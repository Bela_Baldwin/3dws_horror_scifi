﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RotateObject : MonoBehaviour
{
    [Header("Settings for the Object itself")]
    [SerializeField]
    protected float rotSpeed = 5f;
    [SerializeField]
    protected string rotXAxis = "Mouse Y", rotYAxis = "Mouse X";
    [SerializeField]
    protected new MeshRenderer renderer;
    [SerializeField]
    protected MeshFilter meshFilter;
    [SerializeField]
    protected Transform objectTransform;
    [SerializeField]
    protected Transform destinationTransform;

    [Header("In and Out transition")]
    [SerializeField]
    protected float transitionTime;
    [SerializeField]
    protected AnimationCurve lerpCurve;

    [Header("UI")]
    [SerializeField]
    protected GameObject viewObjectUI;
    [SerializeField]
    protected TextMeshProUGUI objectText;
    //[SerializeField]
    //protected Vector2 blur = new Vector2(0.2f, 5f);
    [SerializeField]
    protected Vector2 bgAlpha = new Vector2(0.0f, 0.6f);
    [SerializeField]
    protected Image bgImage;
    [SerializeField]
    protected Slider zoomSlider;
    [SerializeField, Header("Font")]
    protected string altFont;

    [Header("Object Camera")]
    [SerializeField]
    protected new Camera camera;
    [SerializeField]
    protected Vector2 minmaxFOV = new Vector2(40f, 60f);
    [SerializeField]
    protected string zoomAxis = "Mouse Wheel";
    [SerializeField]
    protected float zoomSpeed = 6;
    [Header("Audio")]
    [SerializeField]
    protected AudioSource audioSource;
    [SerializeField]
    protected AudioClip revealSound;

    public static event System.Action OnLeaveViewMode;

    protected bool activeControls = false;
    protected ViewableObject actualWorldObject;
    protected Vector3 objToCamera;
    protected Quaternion objToCameraRotation;

    public Mesh Mesh { get { return meshFilter.sharedMesh; } set { meshFilter.sharedMesh = value; } }
    public Material[] Materials { get { return renderer.sharedMaterials; } set { renderer.sharedMaterials = value; } }

    public Vector3 LocalPosition { set { objectTransform.localPosition = value; } }
    public Vector3 Scale { set { objectTransform.localScale = value; } }
    public Quaternion Rotation { set { objectTransform.rotation = value; } }

    //Setup slider n stuff
    private void Start()
    {
        if (!audioSource)
            audioSource = GetComponent<AudioSource>();
        audioSource.clip = revealSound;
        zoomSlider.minValue = minmaxFOV.x;
        zoomSlider.maxValue = minmaxFOV.y;
        zoomSlider.value = camera.fieldOfView;
        //Add listener. This is fine because they are activated and de-activated together no matter what
        zoomSlider.onValueChanged.AddListener((value) => camera.fieldOfView = value);
    }

    void Update()
    {
        if (!activeControls)
            return;

        DoZoom();

        if(Input.GetKeyDown(KeyCode.Escape)) //go out of the view.
        {
            activeControls = false;
            StartCoroutine(FinalizeDisable());
        }

        DoRotation();
        CheckHiddenDescription();
        //update the slider.
        zoomSlider.value = camera.fieldOfView;
    }
    
    void DoZoom()
    {
        //adjust zoom
        camera.fieldOfView = Mathf.Clamp(camera.fieldOfView - Input.GetAxis(zoomAxis) * zoomSpeed, minmaxFOV.x, minmaxFOV.y);
    }

    void DoRotation()
    {
        //mouse movement only detected when in the left 80% of the screen, to give the zoom slider some space to work
        if (Input.mousePosition.x > Screen.width * 0.8)
            return;

        //when pressed, lock the mouse and hide it
        if (Input.GetMouseButtonDown(0))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        } //when released, unlock and show it
        else if (Input.GetMouseButtonUp(0))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        } //while its down, rotate the object.
        if (Input.GetMouseButton(0))
        {
            Vector2 rotation;
            rotation.x = Input.GetAxis(rotXAxis);
            rotation.y = -Input.GetAxis(rotYAxis);
            //rotation.Normalize();
            if (rotation.sqrMagnitude <= 0.01f)
                return;

            objectTransform.Rotate(rotation * rotSpeed, Space.World);
        }
    }

    void CheckHiddenDescription()
    {
        if (!actualWorldObject.HasSecret)
            return;
        if (actualWorldObject.HiddenSeen)
            return;
        //the current hidden feature direction.
        Vector3 currentDirection = objectTransform.TransformDirection(actualWorldObject.hiddenFeatureDirection);
        if (Vector3.Dot(currentDirection, Vector3.forward) <= -0.9f)
        {
            audioSource.Play();
            actualWorldObject.HiddenSeen = true;
            objectText.text = ParseText(actualWorldObject.HiddenDescription);
        }

    }

    public void Setup(ViewableObject source, Vector3 relativeToCam)
    {
        //reset FOV
        camera.fieldOfView = minmaxFOV.y;
        zoomSlider.value = minmaxFOV.y;

        objectTransform.localPosition = relativeToCam; 
        //print(source.RendererTransform.localScale.x);
        objToCameraRotation = objectTransform.rotation;
        //print(relativeToCam);

        objToCamera = relativeToCam;
        actualWorldObject = source; //disable the source GO and re-enable it when this turns off.
        actualWorldObject.gameObject.SetActive(false);

        //set the basic stuff
        this.Mesh = source.Mesh;
        this.Materials = source.Materials;
        this.Scale = source.RendererTransform.localScale;

        //activate UI
        viewObjectUI.SetActive(true);
        objectText.text = ParseText(source.HiddenSeen? source.HiddenDescription : source.Description);

        //activate self
        gameObject.SetActive(true);
        StartCoroutine(FinalizeEnable());
    }

    //smoothly enter
    IEnumerator FinalizeEnable()
    {
        var startPos = objectTransform.localPosition;
        Blur.SetActive(true);
        var startColor = bgImage.color;
        for(float t = 0; t < transitionTime; t += Time.deltaTime)
        {
            float normT = t / transitionTime;
            float finalT = lerpCurve.Evaluate(normT);
            objectTransform.localPosition = Vector3.Lerp(startPos, destinationTransform.localPosition, finalT);
            //! doesnt work: Blur.SetBlurStrength(Mathf.Lerp(blur.x, blur.y, finalT));
            //print(normT + " --- " + lerpCurve.Evaluate(normT));
            startColor.a = Mathf.Lerp(bgAlpha.x, bgAlpha.y, finalT);
            bgImage.color = startColor; //set the color
            yield return null;
        }
        activeControls = true;
    }

    //smoothly exit
    IEnumerator FinalizeDisable()
    {
        var startPos = objectTransform.localPosition;
        var lastRotation = objectTransform.rotation;
        var startColor = bgImage.color;
        var startFOV = camera.fieldOfView;
        for (float t = 0; t < transitionTime; t += Time.deltaTime)
        {
            float normT = t / transitionTime;
            float finalT = lerpCurve.Evaluate(normT);
            objectTransform.localPosition = Vector3.Lerp(startPos, objToCamera, finalT);
            objectTransform.rotation = Quaternion.Lerp(lastRotation, objToCameraRotation, finalT);
            //! doesnt work: Blur.SetBlurStrength(Mathf.Lerp(blur.y, blur.x, finalT));
            startColor.a = Mathf.Lerp(bgAlpha.y, bgAlpha.x, finalT);
            bgImage.color = startColor; //set the color
            //also lerp back to default FOV!
            camera.fieldOfView = Mathf.Lerp(startFOV, minmaxFOV.y, finalT);
            yield return null;
        }
        camera.fieldOfView = minmaxFOV.y;
        actualWorldObject.gameObject.SetActive(true);
        viewObjectUI.SetActive(false);
        //Invoke static OnLeaveViewMode event
        OnLeaveViewMode?.Invoke();
        Blur.SetActive(false); //disable blur effect
        gameObject.SetActive(false);//disable self
    }

    string ParseText(string input)
    {
        System.Text.StringBuilder output = new System.Text.StringBuilder();

        var parts = input.Split('\"');

        bool startTag = true;
        for(int i = 0; i < parts.Length; i++)
        {
            //check for " char
            output.Append(parts[i]);
            if (i < parts.Length - 1)
            {
                output.Append(startTag ? $"<font=\"{altFont}\">" : "</font>");
                startTag = !startTag;
            }

        }
        if(!startTag)
            output.Append("</font>");
        return output.ToString();
    }
}