﻿using UnityEngine;
using System.Collections;

namespace Horror
{
    [RequireComponent(typeof(Animator)), AddComponentMenu("Trigger/Animation")]
    public class AnimationTrigger : TriggerComponent
    {
        [SerializeField] 
        protected string animationName;
        [SerializeField]
        protected Animator animator;

        protected override void Start()
        {
            base.Start();
            var animator = GetComponent<Animator>();
        }

        protected override void Trigger(Collider other) => animator.Play(animationName);
    }
}