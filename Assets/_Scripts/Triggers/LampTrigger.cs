﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Horror
{
    [RequireComponent(typeof(TriggerBox)), AddComponentMenu("Trigger/Lamp")]
    public class LampTrigger : TriggerComponent
    {
        [SerializeField]
        protected TriggerMode disableLight;
        [SerializeField]
        protected GameObject lamp;

        protected override void Start()
        {
            trigger = GetComponent<TriggerBox>();
            //setup enable light events
            if (mode == TriggerMode.OnEnter)
                trigger.OnPlayerEnter += (Collider other) => lamp.SetActive(true);
            else if (mode == TriggerMode.OnExit)
                trigger.OnPlayerExit += (Collider other) => lamp.SetActive(true);
            //setup disable light events 
            if (disableLight == TriggerMode.OnEnter)
                trigger.OnPlayerEnter += (Collider other) => lamp.SetActive(false);
            else if (disableLight == TriggerMode.OnExit)
                trigger.OnPlayerExit += (Collider other) => lamp.SetActive(false);
        }

        protected override void Trigger(Collider other)
        {
            //not used uhhh
        }

        public override void DrawGizmos()
        {
            if (!lamp.activeSelf)
                Gizmos.DrawIcon(lamp.transform.position, "PointLight Gizmo");
            //revert to global matrix?
            Gizmos.DrawLine(transform.position, lamp.transform.position);
        }
    }
}
