﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Horror
{
    [AddComponentMenu(menuName: "Trigger/Monologue")]
    public class MonologueTrigger : TriggerComponent
    {
        [SerializeField]
        protected MonologueContent content;
        
        protected override void Trigger(Collider other)
        {
            other.GetComponent<Player>().DoMonologue(content);
            if (mode == TriggerMode.OnEnter)
                trigger.OnPlayerEnter -= Trigger;
                trigger.OnPlayerExit -= Trigger;
        }
        
    }
}