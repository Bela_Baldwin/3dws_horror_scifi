﻿using UnityEngine;
using System.Collections;

namespace Horror
{
    [RequireComponent(typeof(TriggerBox))]
    public abstract class TriggerComponent : MonoBehaviour
    {
        [SerializeField]
        protected TriggerMode mode;

        protected TriggerBox trigger;
        
        // Use this for initialization
        protected virtual void Start()
        {
            trigger = GetComponent<TriggerBox>();

            if (mode == TriggerMode.OnEnter)
                trigger.OnPlayerEnter += Trigger;
            else if (mode == TriggerMode.OnExit)
                trigger.OnPlayerExit += Trigger;
        }

        protected abstract void Trigger(Collider other);
        public virtual void DrawGizmos() { }
    }
}