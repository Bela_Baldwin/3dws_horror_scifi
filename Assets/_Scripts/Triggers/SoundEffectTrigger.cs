﻿using UnityEngine;
using System.Collections;

namespace Horror
{
    [AddComponentMenu("Trigger/SoundEffect")]
    public class SoundEffectTrigger : TriggerComponent
    {
        [SerializeField]
        protected AudioSource source;
        [SerializeField]
        protected AudioClip[] clips;
        [SerializeField, Range(0f, 1f)]
        protected float pitchDeviation;
        [SerializeField]
        protected bool onlyPlayOnce = false;
        [SerializeField]
        protected bool fadeIn = false;
        [SerializeField]
        protected float fadeTime = 0.5f;

        protected bool played; 
        
        void PlaySound()
        {
            if (!source)
                source = gameObject.AddComponent<AudioSource>();
            source.clip = clips[Random.Range(0, clips.Length)];
            source.pitch = 1 + Random.Range(-pitchDeviation, pitchDeviation);
            source.Play();
            if (fadeIn)
                StartCoroutine(FadeIn());
        }

        protected override void Trigger(Collider other)
        {
            if (onlyPlayOnce && played)
                return;
            played = true;
            PlaySound();
        }

        IEnumerator FadeIn()
        {
            for(float t = 0; t < fadeTime; t += Time.deltaTime)
            {
                source.volume = t / fadeTime;
                yield return null;
            }
            source.volume = 1;
        }
    }
}