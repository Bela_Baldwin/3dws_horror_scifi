﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Horror
{
    public enum TriggerMode
    {
        OnExit = 1, OnEnter = 2, Never = 0
    }

    [RequireComponent(typeof(BoxCollider)), AddComponentMenu("Trigger/TriggerBox")]
    public class TriggerBox : MonoBehaviour, ISerializationCallbackReceiver
    {
        [SerializeField]
        protected string playerTag = "Player";
        [SerializeField]
        protected BoxCollider m_collider;
        [SerializeField]
        protected bool showInEditor = true, showComponentGizmos = true;

        [SerializeField, HideInInspector]
        protected TriggerComponent[] components;

        public event System.Action<Collider> OnPlayerEnter;
        public event System.Action<Collider> OnPlayerExit;

        private void OnValidate()
        {
            components = GetComponents<TriggerComponent>();
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (!m_collider || !showInEditor)
                return;
            UnityEditor.Handles.Label(transform.position, name);
            Matrix4x4 matrix = Gizmos.matrix;
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = new Color(0.8f, 0.2f, 0.2f, 0.5f);
            Gizmos.DrawCube(m_collider.center, m_collider.size);
            //reset the matrix
            Gizmos.matrix = matrix;
            //draw components. yeahhh
            if(showComponentGizmos)
                foreach (var comp in components)
                    comp.DrawGizmos();
        }

        [MenuItem("GameObject/3D Object/TriggerBox", false, 30)]
        public static void CreateTriggerBox()
        {
            var x = new GameObject();
            x.AddComponent<TriggerBox>();
            x.name = "New Trigger Box";
            Selection.activeGameObject = x;
            EditorWindow.GetWindow<SceneView>().Focus();
            //works but not really in the way i wanted it to lol
            x.transform.position = SceneView.lastActiveSceneView.camera.ViewportToWorldPoint(Vector3.one / 2f);
        }
#endif

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(playerTag))
            {
                OnPlayerEnter?.Invoke(other);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if(other.CompareTag(playerTag))
            {
                OnPlayerExit?.Invoke(other);
            }
        }

        public void OnBeforeSerialize()
        {
            if (!m_collider)
            {
                m_collider = GetComponent<BoxCollider>();
                m_collider.isTrigger = true;
            }
        }

        public void OnAfterDeserialize()
        {
            //literally dont do anything
        }
    }
}