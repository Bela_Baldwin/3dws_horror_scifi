﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Horror
{
    [RequireComponent(typeof(BoxCollider))]
    public class DoorButton : MonoBehaviour, IInteractable
    {
        [SerializeField]
        protected Animator doorAnimator;
        [SerializeField]
        protected string doorTrigger;
        [SerializeField]
        protected Collectable requiredItem;
        [SerializeField]
        protected AudioSource source;
        [SerializeField]
        protected AudioClip open, close;
        [SerializeField]
        protected string onFailLine = "Hmm i don't have my keycard on me.";

        bool isOpen = false;

        public void Interact(Player p)
        {
            if (!requiredItem)
                TriggerDoor();
            else if (p.HasCollectable(requiredItem))
                TriggerDoor();
            else p.Say(onFailLine);
        }

        void TriggerDoor()
        {
            source.PlayOneShot(isOpen ? close : open);
            isOpen = !isOpen;
            doorAnimator.SetTrigger(doorTrigger);
        }
    }
}