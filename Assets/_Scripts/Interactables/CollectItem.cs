﻿using UnityEngine;

namespace Horror
{
    [RequireComponent(typeof(BoxCollider))]
    public class CollectItem : MonoBehaviour, IInteractable
    {
        [SerializeField]
        protected Collectable item;
 
        public void Interact(Player p)
        {
            p.AddItem(item);
            gameObject.SetActive(false);
        }
    }
}
