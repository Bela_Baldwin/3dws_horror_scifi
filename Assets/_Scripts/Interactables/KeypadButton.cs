﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Horror
{
    [RequireComponent(typeof(BoxCollider))]
    public class KeypadButton : MonoBehaviour, IInteractable
    {
        [SerializeField]
        protected Keypad pad;
        [SerializeField]
        protected char num;

        public void Interact(Player p)
        {
            if(pad.Register(num))
            {
                p.EndGame();
            }
        }
    }
}