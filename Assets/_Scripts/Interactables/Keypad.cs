﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Horror
{
    public class Keypad : MonoBehaviour
    {
        [SerializeField]
        protected TextMeshPro textArea;
        [SerializeField]
        protected string solution = "3849";
        [SerializeField]
        protected UnityEngine.Events.UnityEvent onSolve;

        private void Start()
        {
            textArea.text = "";
        }

        public bool Register(char n)
        {
            textArea.text += n;
            if(textArea.text.Length == 4)
            {
                if (textArea.text.Equals(solution))
                {
                    onSolve?.Invoke();
                    return true;
                }
                else
                    textArea.text = "";
            }
            return false;

        }
    }
}
