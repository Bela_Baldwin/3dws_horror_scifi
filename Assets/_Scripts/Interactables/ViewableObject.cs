﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ViewableObject : MonoBehaviour, IInteractable
{
    [SerializeField, TextArea(3, 5)]
    protected string description;
    [SerializeField]
    protected MeshFilter meshFilter;
    [SerializeField]
    protected new MeshRenderer renderer;
    /// <summary>
    /// The direction of the hidden feature in local space. 
    /// To get the world-space version, convert it using transform.TransformDirection.
    /// </summary>
    [SerializeField]
    protected bool hasSecret = true;
    [SerializeField, HideInInspector]
    public Vector3 hiddenFeatureDirection = Vector3.up;
    [SerializeField, TextArea(3, 5)]
    protected string hiddenDescription;

    public Mesh Mesh => meshFilter.mesh;
    public Material[] Materials => renderer.sharedMaterials;
    public Transform RendererTransform => renderer.transform;
    public string Description => description;
    public string HiddenDescription => hiddenDescription;
    public Vector3 Scale => transform.localScale;
    public bool HiddenSeen { get; set; } = false;
    public bool HasSecret => hasSecret;

    public virtual void Interact(Player p)
    {
        p.SetupViewObject(this);
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(ViewableObject))]
    public class ViewObjectEditor : Editor
    {
        private void OnSceneGUI()
        {
            var vo = target as ViewableObject; //set up target.
            if (!vo.HasSecret) //only do all this shit if it has a secret.
                return;
            //relative direction from the center of the object.
            var relativePosition = vo.transform.TransformDirection(vo.hiddenFeatureDirection);
            Handles.color = Color.green;
            //draw the label of the hidden feature (for better understanding)
            Handles.Label(vo.transform.position + relativePosition, "Hidden Feature", EditorStyles.boldLabel);
            //Draw a line to visualize the direction
            Handles.DrawLine(vo.transform.position, vo.transform.position+relativePosition);
            //Get the new position of the handle
            var posX = Handles.DoPositionHandle(vo.transform.position + relativePosition, Quaternion.identity);
            var dir = posX - vo.transform.position; //turn it into a direction
            //normalize the direction
            dir.Normalize();
            //inverse transform the direction from worldspace no local space.
            vo.hiddenFeatureDirection = vo.transform.InverseTransformDirection(dir);
        }
    }
#endif
}