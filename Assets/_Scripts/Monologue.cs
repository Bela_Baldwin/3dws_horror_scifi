﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Horror
{
    public class Monologue : MonoBehaviour
    {
        [SerializeField]
        protected GameObject linePrefab;
        [SerializeField]
        protected float inOutTime, stayTime;
        [SerializeField]
        protected AnimationCurve lerpCurve;
        [SerializeField]
        protected float inStartY, outEndY;

        //currently active lines
        protected List<Line> activeLines = new List<Line>();
        //current calculated maxY
        protected float maxY;

        //Update all lines positions, lerp in the ones with low lifetime.
        private void LateUpdate()
        {
            UpdatePositions();
        }

        void UpdatePositions()
        {
            maxY = 0;
            var dt = Time.deltaTime;
            foreach(var line in activeLines)
            {
                line.lifetime += dt;
                if (line.lifetime > stayTime + inOutTime)
                    LerpOut(line);
                else if (line.lifetime < inOutTime)
                    LerpIn(line);
                else
                    MoveToTop(line);
            }
        }

        //Lerp this from the top of the list to outside of it.
        void LerpOut(Line line)
        {
            Vector2 pos = line.transform.anchoredPosition;
            float halfHeight = line.transform.rect.height / 2f;
            float startY = 0f - halfHeight;
            //progress of the lerp
            float t = (line.lifetime - stayTime - inOutTime) / inOutTime;
            if(t >= 1)
            {
                activeLines.Remove(line);
                Destroy(line.transform.gameObject);
                return;
            }
            t = lerpCurve.Evaluate(1f - t); //"inverse" t
            //lerp to the out pos.
            pos.y = Mathf.Lerp(outEndY, startY, t); //inverse lerp because inverse t
            //apply position.
            line.transform.anchoredPosition = pos;
            //fade out alpha
            line.gui.alpha = t;
            //adjust maxY for following elements.
            maxY = Mathf.Min(maxY, pos.y - halfHeight);
        }

        //move the line from the very bottom to the center.
        void LerpIn(Line line)
        {
            //progress of the lerp
            float t = line.lifetime / inOutTime;
            t = lerpCurve.Evaluate(t);
            //position.
            Vector2 pos = line.transform.anchoredPosition;
            float maxYMHH = maxY - line.transform.rect.height / 2f;
            pos.y = Mathf.Lerp(inStartY, maxYMHH, t);
            line.transform.anchoredPosition = pos;
            //fade in alpha
            line.gui.alpha = t;
        }

        //move the current line to the highest position it can currently go to.
        void MoveToTop(Line line)
        {
            //get the current position
            var pos = line.transform.anchoredPosition;
            //set the y position, x stays
            pos.y = maxY - line.transform.rect.height / 2f;
            //aply the change.
            line.transform.anchoredPosition = pos;
            //adjust the maxY to be below the current rect.
            maxY -= line.transform.rect.height;
        }

        public void Say(string vLine)
        {
            if (activeLines.Find(x => x.gui.text.Equals(vLine)) is null)
            {
                var temp = Instantiate(linePrefab, transform);
                Line line = new Line(temp.transform as RectTransform);
                line.gui.text = vLine;
                activeLines.Add(line);
            }
        }

        public void StartMono(MonologueContent content)
         => StartCoroutine(DoMonologue(content));

        IEnumerator DoMonologue(MonologueContent content)
        {
            foreach(var p in content.parts)
            {
                Say(p.line);
                yield return new WaitForSeconds(p.delay);
            }
        }

        public class Line
        {
            public RectTransform transform;
            public float lifetime;
            public TextMeshProUGUI gui;

            public GameObject GObject => transform.gameObject;

            public Line(RectTransform t)
            {
                transform = t;
                lifetime = 0f;
                gui = transform.GetComponent<TextMeshProUGUI>();
                gui.alpha = 0f; //set alpha to 0 at the start.
            }
        }
    }
}