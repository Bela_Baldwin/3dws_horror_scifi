﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using Horror;

public class Player : MonoBehaviour
{
    [SerializeField]
    protected Camera cam;
    [SerializeField]
    protected NavMeshAgent agent;
    [SerializeField]
    protected float speed;
    [SerializeField]
    protected float minXRot = -70f, maxXRot = 80f;
    [SerializeField]
    protected Vector2 rotationSpeed;
    [SerializeField]
    protected string rotXAxis = "Mouse Y", rotYAxis = "Mouse X", movXAxis = "Horizontal", movZAxis = "Vertical";

    [Header("interactions")]
    [SerializeField]
    protected RotateObject viewObject;
    [SerializeField]
    protected LayerMask raycastMask;
    [SerializeField]
    protected float rayLength;
    [SerializeField]
    protected RenderTexture renderTex;
    [SerializeField]
    protected UnityEngine.UI.Image center;

    [SerializeField, Header("Mono")]
    protected Monologue monologueUI;

    [SerializeField, Header("Footsteps")]
    protected float stepLength = 0.6f;
    [SerializeField]
    protected AudioSource footAudioSource;
    [SerializeField]
    protected AudioClip[] footstepClips;

    [Header("Walking Settings")]
    [SerializeField]
    protected float headbobDistance = 0.07f;
    [SerializeField]
    protected AnimationCurve bobCurve;

    protected bool isMoving = false;
    protected float stepProgress = 0f;
    protected Vector3 lastPos;

    protected bool activeControls = true;
    protected float currentXRot = 0;
    Vector3 baseOffset;

    //inventory, very basic.
    protected HashSet<Collectable> inventory = new HashSet<Collectable>();
    
    private void Start()
    {
        baseOffset = cam.transform.localPosition;
        lastPos = transform.position;

        renderTex.height = Screen.height;
        renderTex.width = Screen.width;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void StartFromAnimation()
    {
        print("ree");
        GetComponent<Animator>().enabled = false;
    }

    public void Say(string msg)
        =>  monologueUI.Say(msg);

    //smart boi
    private void Update()
    {
        if (!activeControls)
            return;

        RotateCamera();
        Raycast();
        Move();
        DoHeadBob();
    }

    void RotateCamera()
    {
        //rotate freely on x
        transform.Rotate(0, Input.GetAxis(rotYAxis) * rotationSpeed.x, 0);

        //get rotation value
        float deltaCamRot = -Input.GetAxis(rotXAxis) * rotationSpeed.y;
        if (Mathf.Approximately(deltaCamRot, 0f))
            return;
        //clamp rotation
        deltaCamRot = Mathf.Clamp(deltaCamRot, minXRot - currentXRot, maxXRot - currentXRot);
        currentXRot += deltaCamRot;
        //rotate
        cam.transform.Rotate(deltaCamRot, 0, 0);
    }
    void Raycast()
    {
        //setup the ray.
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit, rayLength, raycastMask))
        {
            var interact = hit.transform.GetComponent<IInteractable>();
            if (interact != null)
            {
                center.color = Color.green;
                if (Input.GetMouseButtonDown(0))
                    interact.Interact(this);
            }
            else
                center.color = Color.white;
        }
    }

    public void SetupViewObject(ViewableObject source)
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        //get the correct relative position to the camera.
        Vector3 relativePosition = cam.transform.InverseTransformPoint(source.transform.position);
        //print(relativePosition);

        //calculate the correct rotation
        var up = cam.transform.InverseTransformDirection(source.transform.up);
        var forward = cam.transform.InverseTransformDirection(source.transform.forward);
        
        viewObject.Rotation = Quaternion.LookRotation(forward, up);

        //enable the camera n stuff.
        viewObject.Setup(source, relativePosition);
        //disable this 
        activeControls = false;
    }

    //OKAY this works, now time to clean up this piece of shit
    private void Move()
    {
#if UNITY_EDITOR
        speed += Input.GetAxis("Mouse ScrollWheel");
#endif
        //why the hell doesnt forward work anymore
        Vector3 input = new Vector3();
        input.x = Input.GetAxis(movXAxis); input.z = Input.GetAxis(movZAxis);
        if( 0.2f > input.sqrMagnitude) //no input basically.
        {
            if(isMoving)
            {
                isMoving = false;
                footAudioSource.PlayOneShot(footstepClips[Random.Range(0, footstepClips.Length)]);
                stepProgress = 0f;
            }
            return;
        }
        isMoving = true;
        stepProgress += Vector3.Distance(lastPos, transform.position);
        if(stepProgress >= stepLength)
        {
            stepProgress -= stepLength;
            footAudioSource.PlayOneShot(footstepClips[Random.Range(0, footstepClips.Length)]);
        }
        lastPos = transform.position;
        input = input.x * transform.right + input.z * transform.forward;
        input.Normalize();
        input *= speed * Time.deltaTime;
        //use the navmesh agent instead of the rigidbody idgaf.
        agent.Move(input);

    }
    
    //Do the bobbing
    void DoHeadBob()
    {
        var t = stepProgress / stepLength;
        //distance to be offset
        var bobOffset = (-1f + bobCurve.Evaluate(t)) * headbobDistance;
        var offsetLocal = baseOffset + new Vector3(0, bobOffset);
        cam.transform.localPosition = offsetLocal;
    }

    private void OnEnable()
    {
        RotateObject.OnLeaveViewMode += EnableControls;
    }

    private void OnDisable()
    {
        RotateObject.OnLeaveViewMode -= EnableControls;
    }

    void EnableControls()
    {
        activeControls = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public bool HasCollectable(Collectable item)
        => inventory.Contains(item);

    public void AddItem(Collectable item)
        => inventory.Add(item);

    public void DoMonologue(MonologueContent content) => monologueUI.StartMono(content);

    public void EndGame()
    {
        //TODO: This.
        gameObject.SetActive(false);
    }
}