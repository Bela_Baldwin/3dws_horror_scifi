﻿using UnityEngine;
using System.Collections.Generic;

namespace Horror
{
    [CreateAssetMenu(fileName = "new Monologue", menuName = "Game SO/Monologue")]
    public class MonologueContent : ScriptableObject
    {
        public List<Part> parts;

        [System.Serializable]
        public class Part
        {
            public string line;
            public float delay = 1f;
        }
            
    }
}