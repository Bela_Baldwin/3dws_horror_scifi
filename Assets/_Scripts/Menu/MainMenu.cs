﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    protected new Transform camera;
    [SerializeField]
    protected Transform settingsTransform, defaultTransform, playTransform;
    [SerializeField]
    protected float transitionTime;
    [SerializeField]
    protected AnimationCurve transCurve, alphaCurve;
    [SerializeField]
    protected CanvasGroup settingsGroup, menuGroup;
    [SerializeField]
    protected string playScene = "MainScene";

    public void GoToSettings()
    {
        StartCoroutine(DoSettings());
    }

    public void GoToMenu()
    {
        StartCoroutine(DoMenu());
    }

    public void Play()
    {
        StartCoroutine(DoPlay());
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator DoPlay()
    {
        for (float t = 0f; t < transitionTime; t += Time.deltaTime)
        {
            float normT = t / transitionTime;
            float curveT = transCurve.Evaluate(normT);
            camera.rotation = Quaternion.Lerp(defaultTransform.rotation, playTransform.rotation, curveT);
            camera.position = Vector3.Lerp(defaultTransform.position, playTransform.position, curveT);

            menuGroup.alpha = 1f - alphaCurve.Evaluate(normT);
            yield return null;
        }
        SceneManager.LoadScene(playScene);
    }

    IEnumerator DoMenu()
    {
        settingsGroup.interactable = false;
        settingsGroup.blocksRaycasts = false; //oops
        for (float t = 0f; t < transitionTime; t+= Time.deltaTime)
        {
            float normT = t / transitionTime;
            float curveT = transCurve.Evaluate(normT);
            camera.rotation = Quaternion.Lerp(settingsTransform.rotation, defaultTransform.rotation, curveT);
            camera.position = Vector3.Lerp(settingsTransform.position, defaultTransform.position, curveT);

            menuGroup.alpha = alphaCurve.Evaluate(normT);
            yield return null;
        }
        menuGroup.interactable = true;
        menuGroup.blocksRaycasts = true;
        menuGroup.alpha = 1f;
    }

    IEnumerator DoSettings()
    {
        menuGroup.interactable = false;
        menuGroup.blocksRaycasts = false;
        for (float t = 0f; t < transitionTime; t += Time.deltaTime)
        {
            float normT = t / transitionTime;
            float curveT = transCurve.Evaluate(normT);
            camera.rotation = Quaternion.Lerp(defaultTransform.rotation, settingsTransform.rotation, curveT);
            camera.position = Vector3.Lerp(defaultTransform.position, settingsTransform.position, curveT);

            menuGroup.alpha = 1f - alphaCurve.Evaluate(normT);
            yield return null;
        }
        menuGroup.alpha = 0f;
        settingsGroup.interactable = true;
        settingsGroup.blocksRaycasts = true;
    }
}
