﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.UI;

namespace Horror
{
    public class Settings : MonoBehaviour
    {
        [SerializeField]
        protected AudioMixer mixer;
        [SerializeField]
        protected string bgVolume = "BGVolume", sfxVolume = "SFXVolume", masterVolume = "MasterVolume";
        [SerializeField]
        protected Slider bgSlider, sfxSlider, masterSlider;
        [SerializeField]
        protected Vector2 volumeMinMax = new Vector2(-80f, 0f); //the values that will be sent to the audiomixer
        [SerializeField]
        protected RectTransform masterBGConnector, bgSfxConnector;

        //set up the stuff.
        private void Start()
        {
            //step 0: get all the values.
            float bgVol = PlayerPrefs.GetFloat(bgVolume, 1f);
            float sfxVol = PlayerPrefs.GetFloat(sfxVolume, 1f);
            float masterVol = PlayerPrefs.GetFloat(masterVolume, 1f);
            
            //step 1: correct the current values of the sliders.
            bgSlider.value = bgVol;
            sfxSlider.value = sfxVol;
            masterSlider.value = masterVol;

            //step 2: send the adjusted volumes to the mixer.
            mixer.SetFloat(bgVolume, GetRealVolume(bgVol));
            mixer.SetFloat(sfxVolume, GetRealVolume(sfxVol));
            mixer.SetFloat(masterVolume, GetRealVolume(masterVol));

            //step 3: add listeners to the sliders.
            bgSlider.onValueChanged.AddListener((value) => UpdateFloat(bgVolume, value));
            sfxSlider.onValueChanged.AddListener((value) => UpdateFloat(sfxVolume, value));
            masterSlider.onValueChanged.AddListener((value) => UpdateFloat(masterVolume, value));

            //step4: update the line.
            UpdateLine(); 
        }

        //a helper to make the code more clean.
        void UpdateFloat(string name, float value)
        {
            mixer.SetFloat(name, GetRealVolume(value));
            UpdateLine();
            PlayerPrefs.SetFloat(name, value);
            PlayerPrefs.Save();
        }

        void UpdateLine()
        {

            var masterRect = masterSlider.handleRect;
            var bgRect = bgSlider.handleRect;
            var sfxRect = sfxSlider.handleRect;

            //masterBG connector
            masterBGConnector.position = Vector3.Lerp(masterRect.position, bgRect.position, 0.5f);
            masterBGConnector.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Vector3.Distance(masterRect.position, bgRect.position) * 100);
            masterBGConnector.rotation = Quaternion.LookRotation(Vector3.down, (bgRect.position - masterRect.position).normalized);
            //bg connector.
            bgSfxConnector.position = Vector3.Lerp(bgRect.position, sfxRect.position, 0.5f);
            bgSfxConnector.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Vector3.Distance(bgRect.position, sfxRect.position) * 100);
            bgSfxConnector.rotation = Quaternion.LookRotation(Vector3.down,(sfxRect.position - bgRect.position).normalized);
        }
        
        float GetRealVolume(float value) //if the value is very close to 0, return -80db (basically muted), otherwise the lerp thing
            => value < 0.01f ? -80f : Mathf.Lerp(volumeMinMax.x, volumeMinMax.y, Mathf.Log(value)+1);

    }
}