﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blur : MonoBehaviour
{
    protected static Blur _instance;

    [SerializeField]
    protected Material material;

    private void Start()
    {
        _instance = this;
        enabled = false;
    }

    public static void SetBlurStrength(float value)
    {
        if(!_instance)
        {
            Debug.LogError("Tried to set blur strength without blur in the scene!");
            return;
        }
        _instance.material.SetFloat("Radius", value);
    }

    public static void SetActive(bool active)
    {
        if (!_instance)
            return;
        _instance.enabled = active;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material); //blit
    }
}
