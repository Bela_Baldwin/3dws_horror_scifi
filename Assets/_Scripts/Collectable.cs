﻿using System.Collections.Generic;
using UnityEngine;

namespace Horror
{
    [CreateAssetMenu(fileName = "new Collectable", menuName = "Game SO/Collectable")]
    public class Collectable : ScriptableObject
    {
        public string id;
    }
}
