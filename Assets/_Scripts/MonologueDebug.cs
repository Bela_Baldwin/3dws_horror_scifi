﻿using UnityEngine;
using System.Collections;
using Horror;

namespace Horror.Testing
{
    public class MonologueDebug : MonoBehaviour
    {
        public MonologueContent content;

        // this is just testing.
        void Start()
        {
            var mono = GetComponent<Monologue>();
            mono.StartMono(content);
        }
        
    }
}