﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Horror
{
    public class AdjustBrightness : MonoBehaviour
    {
        [SerializeField]
        protected PostProcessProfile profile;
        [SerializeField]
        protected string nextScene;
        [SerializeField]
        protected string brightnessSet, brightnessSave;
        [SerializeField]
        protected Slider slider;

        ColorGrading grading;

        private void Start()
        {
            grading = profile.GetSetting<ColorGrading>();
            if(PlayerPrefs.GetInt(brightnessSet, 0) == 1)
            {
                OnValueChange(PlayerPrefs.GetFloat(brightnessSave));
                SceneManager.LoadScene(nextScene);
            }
            slider.onValueChanged.AddListener(OnValueChange);
        }

        public void OnValueChange(float val)
        {
            grading.postExposure.value = val;
        }

        public void Confirm()
        {
            PlayerPrefs.SetInt(brightnessSet, 1);
            PlayerPrefs.SetFloat(brightnessSave, grading.postExposure.value);
            PlayerPrefs.Save();
            SceneManager.LoadScene(nextScene);
        }

        public void ClearSettings()
        {
            PlayerPrefs.DeleteKey(brightnessSave);
            PlayerPrefs.DeleteKey(brightnessSet);
        }

    }
    #if UNITY_EDITOR
    [CustomEditor(typeof(AdjustBrightness))]
    public class BrightnessEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if(GUILayout.Button("Clear"))
            {
                (target as AdjustBrightness).ClearSettings();
            }
        }
    }
    #endif
}