﻿using UnityEngine;
using UnityEngine.SceneManagement;
namespace Horror
{
    public class LoadMenu : MonoBehaviour
    {
        private void Start()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
