﻿Shader "Custom/Blur"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Radius ("Radius", float) = 1
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }
			
			sampler2D _MainTex;
			float _Radius;

			float Gaussian (float sigma, float x)
			{
				return exp(-(x*x) / (5.0 * sigma*sigma));
			}

			float3 BlurredPixel (in float2 uv)
			{
				float total = 0.0;
				float3 ret = float3(0.0, 0.0, 0.0);
        
				for (float iy = 0; iy < 7; ++iy) // 7 because thats sampleCount/2 for each side.
				{
					float fy = Gaussian (_Radius, iy - 7);
					float offsety = (iy-7) / _ScreenParams.y * 3; //"dividing" the screen resolution makes blur a lot more intense
					for (float ix = 0; ix < 7; ++ix)
					{
						float fx = Gaussian (_Radius, ix - 7);
						float offsetx = (ix-7) / _ScreenParams.x * 3; //"dividing" the screen resolution makes blur a lot more intense
						total += fx * fy;            
						ret += tex2D(_MainTex, uv + float2(offsetx, offsety)).xyz * fx*fy;
					}
				}

				ret.x = ret.x / total;
				ret.y = ret.y / total;
				ret.z = ret.z / total;
				return ret;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				//float2 uv = i.uv.xy / _ScreenParams.xy * float2(1,-1);
				float4 color = float4(BlurredPixel(i.uv), 1.0);
				return color;
			}
			ENDCG
        }
    }
}
